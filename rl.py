from collections.abc import Sequence

from bearlibterminal import terminal
from ecs import Component, EntityManager, System, SystemManager, components_of_type
from bresenham import bresenham
from toolz import sliding_window


COLOR = {
    "blue": "#89CCEE",
    "purple": "#332288",
    "turqoise": "#44AA99",
    "green": "#117733",
    "brown": "#999933",
    "yellow": "#DDCC77",
    "orange": "#CC6677",
    "red": "#882255",
    "pink": "#AA4499",
    "white": "#EEEEEE",
    "black": "#000000",
    "grey": "#191919",
}


class Collision(Component):
    def __init__(self, *, blocks=None):
        if blocks is None:
            raise Exception("fuck you")
        self.blocks = blocks


class Position(Component):
    def __init__(self, *, x=None, y=None):
        if x is None or y is None:
            raise Exception("fuck you")
        self.x = x
        self.y = y


class Renderable(Component):
    def __init__(self, *, color=None, glyph=None):
        if color is None or glyph is None:
            raise Exception("fuck you")
        self.color = COLOR[color]
        self.glyph = glyph


class Velocity(Component):
    def __init__(self, *, dx=None, dy=None):
        if dx is None or dy is None:
            raise Exception("fuck you")
        self.dx = dx
        self.dy = dy


class MovementSystem(System):
    @components_of_type(Position, Velocity)
    def invoke(self, entity_component_pairs):
        for entity, (pos, vel) in entity_component_pairs:
            pos.x += vel.dx
            pos.y += vel.dy
            vel.dx = 0
            vel.dy = 0


class Tile:
    def __init__(
        self, *, color="white", color_by_name=None, glyph=".", blocks=False, opaque=None
    ):
        self.color = COLOR[color_by_name] if color_by_name else COLOR[color]
        self.glyph = glyph
        self.blocks = blocks
        self.opaque = blocks if opaque is None else opaque

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    @property
    def color_by_name(self):
        return self.color

    @color_by_name.setter
    def color_by_name(self, name):
        self.color = COLOR[name]


class Map(Sequence):
    def __init__(self, *, name="", width=58, height=36):
        self.name = name
        self.width = width
        self.height = height
        self.layout = [
            [
                Tile(color_by_name="brown", glyph="#", blocks=True, opaque=True)
                for y in range(height)
            ]
            for x in range(width)
        ]
        self.offset_x = 0
        self.offset_y = 0
        for x in range(1, width - 1):
            for y in range(1, height - 1):
                self.layout[x][y].update(
                    glyph=".", color_by_name="green", blocks=False, opaque=False
                )

    def __getitem__(self, key):
        return self.layout[key]

    def __len__(self):
        return self.width * self.height


def make_rooms(new_map, x, y, w, h):
    for start, finish in sliding_window(
        2, ((x, y), (x + w, y), (x + w, y + h), (x, y + h), (x, y))
    ):
        for tile in bresenham(*start, *finish):
            new_map[tile[0]][tile[1]].update(
                color_by_name="brown", glyph="#", blocks=True, opaque=True
            )


def make_map(name="", width=58, height=36):
    new_map = Map(name=name, width=width, height=height)
    make_rooms(new_map, 10, 10, 5, 5)
    make_rooms(new_map, 20, 10, 5, 5)
    make_rooms(new_map, 10, 20, 5, 5)
    return new_map


class GameEngine:
    move_input = {
        terminal.TK_L | terminal.TK_KEY_RELEASED: (1, 0),
        terminal.TK_N | terminal.TK_KEY_RELEASED: (1, 1),
        terminal.TK_J | terminal.TK_KEY_RELEASED: (0, 1),
        terminal.TK_B | terminal.TK_KEY_RELEASED: (-1, 1),
        terminal.TK_H | terminal.TK_KEY_RELEASED: (-1, 0),
        terminal.TK_Y | terminal.TK_KEY_RELEASED: (-1, -1),
        terminal.TK_K | terminal.TK_KEY_RELEASED: (0, -1),
        terminal.TK_U | terminal.TK_KEY_RELEASED: (1, -1),
    }

    def __init__(self):
        self.window_width = 80
        self.window_height = 45
        self.screen_width = 58
        self.screen_height = 36
        self.cellsize = "12x12"

        self.initialize_blt()
        self.initialize_systems()
        self.generate_world()

    def initialize_blt(self):
        terminal.open()
        terminal.set(
            "window: size={}x{}, cellsize={}, title='Roguelike';"
            "font: default;"
            "input: filter=[keyboard+];"
            "".format(str(self.window_width), str(self.window_height), self.cellsize)
        )
        terminal.clear()
        terminal.refresh()
        terminal.color(COLOR["white"])

    def initialize_systems(self):
        self.ecs = SystemManager(EntityManager())
        self.ecs.add_system(MovementSystem)

    def generate_world(self):
        map_width = 66
        map_height = 44
        self.current_world = make_map(name="Intro", width=map_width, height=map_height)
        self.pc = self.ecs.manager.create_tagged_entity("pc")
        self.ecs.manager.add_multiple_components(
            self.pc,
            Collision(blocks=True),
            Position(x=map_width // 2, y=map_height // 2),
            Renderable(color="yellow", glyph="@"),
            Velocity(dx=0, dy=0),
        )

    def run(self):
        proceed = True
        while proceed:
            if terminal.has_input():
                key = terminal.read()
                proceed = self.process_input(key)
            self.process_one_tick()

    def process_one_tick(self):
        terminal.clear()
        self.set_offset()
        self.render_map()
        self.render_objects()
        self.ecs.update()
        terminal.refresh()

    def set_offset(self):
        """
        Source: http://www.roguebasin.com/index.php?title=Scrolling_map
        """

        center_x = self.screen_width // 2
        center_y = self.screen_height // 2
        pos = self.ecs.manager.component_for_entity(self.pc, Position)

        if pos.x < center_x:
            offset_x = 0
        elif pos.x > self.current_world.width - center_x:
            offset_x = self.current_world.width - self.screen_width
        else:
            offset_x = pos.x - center_x

        if pos.y < center_y:
            offset_y = 0
        elif pos.y > self.current_world.height - center_y:
            offset_y = self.current_world.height - self.screen_height
        else:
            offset_y = pos.y - center_y

        self.current_world.offset_x = offset_x
        self.current_world.offset_y = offset_y

    def render_map(self):
        offset_x = self.current_world.offset_x
        offset_y = self.current_world.offset_y
        for c_idx, column in enumerate(self.current_world):
            if offset_x <= c_idx < offset_x + self.screen_width:
                for r_idx, tile in enumerate(column):
                    if offset_y <= r_idx < offset_y + self.screen_height:
                        col = c_idx - offset_x
                        row = r_idx - offset_y
                        terminal.print(
                            col, row, "[color={}]{}".format(tile.color, tile.glyph)
                        )

    def render_objects(self):
        """This has to be hardcoded because access to all of the internals is required."""
        offset_x = self.current_world.offset_x
        offset_y = self.current_world.offset_y

        entities = set(self.ecs.manager.components[Renderable].keys())
        entities.intersection_update(self.ecs.manager.components[Position].keys())

        for e in entities:
            render = self.ecs.manager.entities[e][Renderable]
            pos = self.ecs.manager.entities[e][Position]
            terminal.print(
                pos.x - offset_x,
                pos.y - offset_y,
                "[color={}]{}".format(render.color, render.glyph),
            )

    def process_input(self, key):
        if key is terminal.TK_CLOSE:
            return False
        if key == terminal.TK_Q and terminal.check(terminal.TK_SHIFT):
            return False
        elif key == terminal.TK_ESCAPE:
            return False
        else:
            self.in_game_input(key)
        return True

    def in_game_input(self, key):
        if key in self.move_input:
            self.move_pc(*self.move_input[key])

    def move_pc(self, dx, dy):
        pos = self.ecs.manager.component_for_entity(self.pc, Position)
        vel = self.ecs.manager.component_for_entity(self.pc, Velocity)
        to_x = pos.x + dx
        to_y = pos.y + dy
        if not self.current_world[to_x][to_y].blocks:
            vel.dx = dx
            vel.dy = dy

    def cleanup(self):
        terminal.close()


def main():
    game = GameEngine()
    game.run()
    game.cleanup()


if __name__ == "__main__":
    main()
