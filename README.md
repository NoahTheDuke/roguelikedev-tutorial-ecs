# r/RoguelikeDev Does The Complete Roguelike Tutorial

![RoguelikeDev Does the Complete Roguelike Tutorial Event Logo][logo]

[logo]: https://i.imgur.com/ksc9EW3.png

[![Code style: black][black-logo]][black-link]

[black-logo]: https://img.shields.io/badge/code%20style-black-000000.svg
[black-link]: https://github.com/ambv/black

At [r/roguelikedev][rld] we're doing a dev-along following [The Complete Roguelike Tutorial][tutorial]!

[rld]: https://www.reddit.com/r/roguelikedev/
[tutorial]: https://www.reddit.com/r/roguelikedev/wiki/python_tutorial_series

## If you would like to participate on GitLab

* Sign up for a free account with [GitLab][gitlab] if you don't already have one.
* Fork [this repository][this_repo] or the [original repository][original_repo] to your account.
* Clone the repository on your computer and follow the tutorial.
* Follow along with the [weekly posts][reddit].
* Update the `README.md` file to include a description of your game, how/where to play/download it, how to build/compile it, what dependencies it has, etc.
* Share your game on the final week.

[gitlab]: https://gitlab.com/users/sign_in
[this_repo]: https://gitlab.com/noahtheduke/roguelikedev-tutorial-ecs
[original_repo]: https://github.com/aaron-santos/roguelikedev-does-the-complete-roguelike-tutorial
[reddit]: https://www.reddit.com/r/roguelikedev/search?q=TCRT&restrict_sr=on

## It's dangerous to go alone <img src="https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/originals/original_new_logo/logo-square.png" height="40">

If you're **new to Git, GitLab, or version control**:

* [Git Documentation][git-scm] - everything you need to know about version control, and how to get started with Git.
* [GitLab Help][gitlab-help] - everything you need to know about GitLab.

[git-scm]: https://git-scm.com/documentation
[gitlab-help]: https://about.gitlab.com/

## Why GitLab?

It's a good question. I have all of my old projects on GitHub, I've spent my entire programming career on GitHub, and my
cars and computers carry Octocats of all flavors. However, I believe in competition, and I believe in putting my money
and efforts where my mouth leads, so because of a strong stance I've taken personally towards supporting more open-
source companies and efforts I've made in my places of employment for using GitLab, I feel it best to put my new
projects (and my money) there as well.

To be clear, this does not stem from animosity or a desire to spark conflict. I have loved GitHub. I'm just trying
something new.
