from abc import ABCMeta, abstractmethod
from collections import defaultdict, OrderedDict
from uuid import uuid4


class EntityManager:
    """
    component dict {component_type: {entity: [component_instances]}}
    entity dict {entity: {component_type: [component_instances]}}
    """

    def __init__(self):
        self.components = defaultdict(dict)
        self.entities = defaultdict(dict)
        self.entities_to_tags = {}
        self.tags_to_entities = defaultdict(list)

    def create_basic_entity(self):
        entity = str(uuid4())
        self.entities_to_tags[entity] = "-"
        return entity

    def create_tagged_entity(self, tag=None):
        if tag in (None, "-"):
            raise ValueError
        entity = str(uuid4())
        self.entities_to_tags[entity] = tag
        self.tags_to_entities[tag].append(entity)
        return entity

    def delete_entity(self, entity):
        for component_type in self.entities[entity]:
            self.purge_components(entity, component_type)
        del self.entities[entity]

    def add_component(self, entity, component, *, multiple=False):
        component_type = type(component)
        if self.components[component_type].get(entity):
            if not multiple:
                raise Exception("You fucked up.")
            self.components[component_type][entity].bundle(component)
            self.entities[entity][component_type].bundle(component)

        self.components[component_type][entity] = component
        self.entities[entity][component_type] = component

    def add_multiple_components(self, entity, *components):
        for component in components:
            self.add_component(entity, component)

    def remove_component(self, entity, component):
        component_type = type(component)
        self.purge_components(entity, component_type)

    def purge_components(self, entity, component_type):
        try:
            del self.components[component_type][entity]
            if not self.components[component_type]:
                del self.components[component_type]
        except KeyError:
            pass

    def component_for_entity(self, entity, component_type):
        return self.entities[entity].get(component_type, None)

    def entities_with_components(self, *component_types):
        if len(component_types) > 1:
            entities = set(self.entities.keys())
            return entities.intersection(
                *[self.components[c_t].keys() for c_t in component_types]
            )
        else:
            return self.components[component_types[0]].keys()

    def get_entities_by_tag(self, tag):
        return self.tags_to_entities[tag]


class Component:
    def __init__(self):
        self.guid = str(uuid4())


class System(metaclass=ABCMeta):
    def __init__(self, manager):
        self.manager = manager

    @abstractmethod
    def invoke(self):
        pass


def components_of_type(*component_types):
    def deco_check(f):
        def deco(self):
            if len(component_types) > 1:
                entities = set(self.manager.components[component_types[0]].keys())
                entities.intersection_update(
                    *[
                        self.manager.components[c_t].keys()
                        for c_t in component_types[1:]
                    ]
                )
            else:
                entities = self.manager.components[component_types[0]].keys()
            comps_per = []
            for e in entities:
                comps_per.append(
                    [e, [self.manager.entities[e][c_t] for c_t in component_types]]
                )
            f(self, comps_per)

        return deco

    return deco_check


class SystemManager:
    def __init__(self, manager):
        self.systems = OrderedDict()
        self.manager = manager

    def add_system(self, system_type, *, overwrite=False):
        if system_type in self.systems:
            if overwrite:
                del self.systems[system_type]
            else:
                return
        self.systems[system_type] = system_type(self.manager)

    def remove_system(self, system_type):
        if system_type in self.systems:
            del self.systems[system_type]

    def update(self):
        for system_type, system in self.systems.items():
            system.invoke()


if __name__ == "__main__":
    ecs = EntityManager()
    entity = ecs.create_entity()
    print(entity.guid)
